<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebNotificationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();
Route::group([], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);

    Route::group(['middleware' => 'api'], function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//        Route::get('/push-notificaiton', [WebNotificationController::class, 'index'])->name('push-notificaiton');
        Route::post('/store-token', [WebNotificationController::class, 'storeToken'])->name('store.token');
//        Route::get('/store', [WebNotificationController::class, 'storeToken'])->name('store.token');
//        Route::post('/send-web-notification', [WebNotificationController::class, 'sendWebNotification'])->name('send.web-notification');
//        Route::get('/send', [WebNotificationController::class, 'send'])->name('send.web-notification');
        Route::get('/me', [AuthController::class, 'me']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::get('push-notification', [WebNotificationController::class, 'index']);
        Route::post('sendNotification', [WebNotificationController::class, 'sendNotification'])->name('send.notification');
    });
});
//Route::get('/token', function () {
//    return csrf_token();
//});
