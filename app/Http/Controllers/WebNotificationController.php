<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;


class WebNotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware('api');
    }


    public function index()

    {
        return view('notify');
    }
    public function sendNotification(Request $request)

    {

        $firebaseToken = User::whereNotNull('device_key')->pluck('device_key')->all();



        $SERVER_API_KEY = env('FCM_SERVER_KEY');



        $data = [

            "registration_ids" => $firebaseToken,

            "notification" => [

                "title" => $request->title,

                "body" => $request->body,

            ]

        ];

        $dataString = json_encode($data);



        $headers = [

            'Authorization: key=' . $SERVER_API_KEY,

            'Content-Type: application/json',

        ];



        $ch = curl_init();



        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);



        $response = curl_exec($ch);



        return back()->with('success', 'Notification send successfully.');

    }



//    public function index()
//    {
//        return view('home');
//    }

//    public function storeToken(Request $request)
//    {
//        $user=User::find(1);
//        $user->update(['device_key'=>$request->token]);
////        return  auth('auth:api')->user();
////        return $user;// $user->update(['device_key'=>$request->token]);
//        return response()->json(['Token successfully stored.']);
//    }

//    public function sendWebNotification(Request $request)
//    {
//        $url = 'https://fcm.googleapis.com/fcm/send';
//        $FcmToken = User::whereNotNull('device_key')->pluck('device_key')->all();
//
//        $serverKey = 'AAAAnoy2nIg:APA91bFsoxmwJMEQrhLCIVDoGaq_flYLO_W-P9jV7KYcQT2wC1zcItZlyiEjLkvhp7fM01nj1Yo9UmWLGUYK7qn2aFZ9i4KWFapINVNgZ3yI4hI0WSBnmsUmtbEb9fqp8eHVpdr6pxxb';
//
//        $data = [
//            "registration_ids" => $FcmToken,
//            "notification" => [
//                "title" => $request->title,
//                "body" => $request->body,
//            ]
//        ];
//        $encodedData = json_encode($data);
//
//        $headers = [
//            'Authorization:key=' . $serverKey,
//            'Content-Type: application/json',
//        ];
//
//        $ch = curl_init();
//
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//        // Disabling SSL Certificate support temporarly
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
//        // Execute post
//        $result = curl_exec($ch);
//        if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
//        }
//        // Close connection
//        curl_close($ch);
//        // FCM response
//        dd($result);
//    }
//
//    public function send()
//    {
//        $url = 'https://fcm.googleapis.com/fcm/send';
//        $FcmToken = User::whereNotNull('device_key')->pluck('device_key')->all();
//
//        $serverKey = 'AAAAnoy2nIg:APA91bFsoxmwJMEQrhLCIVDoGaq_flYLO_W-P9jV7KYcQT2wC1zcItZlyiEjLkvhp7fM01nj1Yo9UmWLGUYK7qn2aFZ9i4KWFapINVNgZ3yI4hI0WSBnmsUmtbEb9fqp8eHVpdr6pxxb';
//
//        $data = [
//            "registration_ids" => $FcmToken,
//            "notification" => [
//                "title" => 'hello',
//                "body"  => 'new notify',
//            ]
//        ];
//        $encodedData = json_encode($data);
//
//        $headers = [
//            'Authorization:key=' . $serverKey,
//            'Content-Type: application/json',
//        ];
//
//        $ch = curl_init();
//
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//        // Disabling SSL Certificate support temporarly
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
//        // Execute post
//        $result = curl_exec($ch);
//        if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
//        }
//        // Close connection
//        curl_close($ch);
//        // FCM response
//        dd($result);
//    }






}
